# How to Properly Open a .csv File after Downloading the Results

Once a scraping job is completed successfully, your data is available in your Lobstr dashboard and when you press `Download`, it's downloaded as a `.csv` file to your computer. You can open it in a text editor like `SublimeText` as a `CSV`, or open it in **Microsoft Excel** for a better visual representation.

## The Problem:

![The problem: Unorganized data in csv](https://lobstr-blog-assets.s3.eu-west-3.amazonaws.com/scattered_data_screenshot_ee0a7f5fd5.png)

Sometimes, the extracted data from a Lobstr run may appear **scattered** or not display correctly when you open your `.csv` file in Microsoft Excel. The easiest solutions to this problem is using a **different spreadsheet application**.

## The Solution:

### For Mac Users
If you are a Mac user, you can open the CSV file using Numbers, a spreadsheet application developed by Apple. Follow these steps:

1. Right-click on the .csv file you downloaded.
2. From the context menu, click "Open With."
3. Select "Numbers" from the available options.

### For Windows and Linux Users
If  you are a Windows or Linux user, you can use Google Spreadsheets to open .csv files. Here's how:

1. Open Google Spreadsheets through [Google Drive](https://drive.google.com/) or open a new spreadsheet directly by visiting [https://sheets.new](https://sheets.new).
2. To import your CSV file, click on the "File" menu and then select "Import."

![Import csv to google sheets](https://lobstr-blog-assets.s3.eu-west-3.amazonaws.com/import_csv_to_gsheet1_6716a8d15c.gif)

3. Choose "Upload" and select your CSV file from your local storage.
4. Make sure to select "Replace data at selected cell" and uncheck "Convert text to numbers, dates, and formulas."

![Import csv to google sheets settings](https://lobstr-blog-assets.s3.eu-west-3.amazonaws.com/import_csv_to_gsheet2_70b90bde5c.gif)

5. After importing, select all rows by pressing Ctrl+A (or Cmd+A on Mac).

![Resize the rows](https://lobstr-blog-assets.s3.eu-west-3.amazonaws.com/resize_the_rows_9d20411674.gif)

6. Right-click on the selected rows and choose "Resize rows."
7. Select "Specify row height" and either leave it as the default value (21) or set a row height of your preference.

That's it! You have successfully opened your .csv file in Numbers (for Mac) or Google Spreadsheets (for Windows and Linux).

Happy Scraping!
